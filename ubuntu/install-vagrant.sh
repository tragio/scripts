#!/bin/sh -eux

sudo apt install -y libarchive-tools
curl -fsSL https://releases.hashicorp.com/vagrant/2.2.14/vagrant_2.2.14_linux_amd64.zip --output vagrant.zip
unzip -qq -o vagrant.zip vagrant
sudo mv vagrant /usr/local/bin
rm vagrant.zip
