#!/bin/sh -eux

sudo apt install -y libdouble-conversion3 libpcre2-16-0 libpython2-stdlib libpython2.7-minimal libpython2.7-stdlib libqt5core5a libqt5dbus5 libqt5gui5 \
  libqt5network5 libqt5opengl5 libqt5printsupport5 libqt5svg5 libqt5widgets5 libqt5x11extras5 libsdl1.2debian libxcb-xinerama0 libxcb-xinput0 \
  python-is-python2 python2 python2-minimal python2.7 python2.7-minimal qt5-gtk-platformtheme qttranslations5-l10n gcc make perl
curl -fsSL https://download.virtualbox.org/virtualbox/6.1.18/virtualbox-6.1_6.1.18-142142~Ubuntu~eoan_amd64.deb --output vbox.deb
sudo dpkg -i vbox.deb
rm vbox.deb

# TODO: break this out own own and make an upstream call to install-kvm
sudo apt install libvirt-daemon qemu-kvm bridge-utils virt-manager
