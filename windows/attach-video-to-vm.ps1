<#
.SYNOPSIS
    THIS DOES NOT WORK...DEMO OF PARAMETERIZED PS
.DESCRIPTION
    Detach and attach a physical video card to a Hyper-V VM then reverse it.
.PARAMETER locationPath
    The base name for the output directories.
.PARAMETER vmName
    The base name for vm's that are created.
#>
param([string]$locationPath = "PCIROOT(0)#PCI(1D04)#PCI(0000)",
      [string]$vmName = "windows-10")

Set-VM -Name $vmName -AutomaticStopAction TurnOff

Set-VM -GuestControlledCacheTypes $true -VMName $vmName
Set-VM -LowMemoryMappedIoSpace 3Gb -VMName $vmName
Set-VM -HighMemoryMappedIoSpace 33280Mb -VMName $vmName
Dismount-VMHostAssignableDevice -LocationPath $locationPath -Force
Add-VMAssignableDevice -LocationPath $locationPath -VMName $vmName


#Set automatic stop action to TurnOff
Set-VM -Name $vm -AutomaticStopAction TurnOff
#Enable Write-Combining on the CPU
Set-VM -GuestControlledCacheTypes $true -VMName $vm
#Configure 32 bit MMIO space
Set-VM -LowMemoryMappedIoSpace 3Gb -VMName $vm
#Configure Greater than 32 bit MMIO space
Set-VM -HighMemoryMappedIoSpace 33280Mb -VMName $vm

#Find the Location Path and disable the Device
#Enumerate all PNP Devices on the system
$pnpdevs = Get-PnpDevice -presentOnly
#Select only those devices that are Display devices manufactured by NVIDIA
$gpudevs = $pnpdevs |where-object {$_.Class -like "Display" -and $_.Manufacturer -like "NVIDIA"}
#Select the location path of the first device that's available to be dismounted by the host.
$locationPath = ($gpudevs | Get-PnpDeviceProperty DEVPKEY_Device_LocationPaths).data[0]
#Disable the PNP Device
Disable-PnpDevice  -InstanceId $gpudevs[0].InstanceId

#Dismount the Device from the Host
Dismount-VMHostAssignableDevice -force -LocationPath $locationPath

#Assign the device to the guest VM.
Add-VMAssignableDevice -LocationPath $locationPath -VMName $vmName


# reverse
#Remove the device from the VM
Remove-VMAssignableDevice -LocationPath $locationPath -VMName $vmName
#Mount the device back in the host
Mount-VMHostAssignableDevice -LocationPath $locationPath
