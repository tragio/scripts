#!/bin/bash -e

usage() {
    echo "./map-hdd-to-file.sh [options]"
    echo "Options:"
    echo -e "\t--resource_name [resource_name]\tThe name of the resource you want to attach to a VM.  Enumerate options under /vmfs/devices/disks/"
    echo -e "\t--file_name [file_name]\t\tThe name of the file that exposes the raw resource for a VM to use."
    echo -e "\t-h, --help\t\t\t\t\tThis help"
}
while [ "$1" != "" ]; do
    case $1 in
        --resource_name )   shift
                            resource_name=$1
                            ;;
        --file_name )       shift
                            file_name=$1
                            ;;
        -h | --help )       usage
                            exit
                            ;;
        * )                 usage
                            exit 1
    esac
    shift
done

# make a vmdk file pointing to a raw resource
args=("vmkfstools")
args+=("-r")
args+=("${resource_name}")
args+=("${file_name}")
echo ${args[@]}
eval ${args[@]}
